<?php
 2 // najpierw składnia $zmienna++
 3 
 4 $zmienna = 5;
 5 
 6 echo 'Stan 1: '.($zmienna++).'<br/>';
 7 echo 'Stan 2: '.$zmienna.'<br/><br/>';
 8 
 9 // teraz składnia ++$zmienna
10 echo 'Restart zmiennej...<br/>';
11 $zmienna = 5;
12 
13 echo 'Stan 1: '.(++$zmienna).'<br/>';
14 echo 'Stan 2: '.$zmienna.'<br/>';


