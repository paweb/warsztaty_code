<?php

$tablica[0] = "Wpis numer 0";
$tablica[1] = "Wpis numer 1";
$tablica[2] = "Wpis numer 2";


echo "FOR:".PHP_EOL;
for($i = 0; $i < 10; $i++)
{
	echo $i.PHP_EOL;	
}

echo "WHILE:".PHP_EOL;
$i = 0;
while($i < 10)
{
	echo $i.PHP_EOL;
	$i++;
}

echo "FOREACH:".PHP_EOL;
foreach($tablica as $id => $wartosc) 
{
  echo "Linia #$id: " . $wartosc . PHP_EOL;	 
}
