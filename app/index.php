<?php


require_once 'vendor/autoload.php';

$posts = [
  1 => [
  'title' => 'Dzisiaj napisałem swoją pierwsza aplikacje',
  'content' => 'Wcale nie było tak trudno jak się spodziewałem',
  'author' => 'Paweł Galerczyk',
  'date' => date("d-m-y")
]];


$loader = new Twig_Loader_Filesystem('view/');
$twig = new Twig_Environment($loader, array(
    'cache' => false,
));


echo $twig->render('index.html', array('posts' => $posts));