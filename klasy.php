<?php
class Samochod
{
   public $marka;
   public $model;
                
   public function ustawSamochod($marka, $model)
   {
      $this->marka = $marka;
      $this->model = $model;
   }

   public function pobierzSamochod()
   {
      return $this->marka.' '.$this->model;               
   } 
}


$audi = new Samochod;
$audi->ustawSamochod('Audi', 'A6');

$toyota = new Samochod;
$toyota->ustawSamochod('Toyota', 'Corolla');


echo 'Witaj, jestem '.$audi->pobierzSamochod().PHP_EOL;
echo 'A ja jestem '.$toyota->pobierzSamochod().PHP_EOL;

class Audi extends Samochod
{
   private $kolor;
                
   public function __construct($model)
   {
      $this->marka = 'Audi';
      $this->model = $model;
   } 
   public function setKolor($kolor)
   {
      $this->kolor = $kolor;          
   } 
   public function pobierzSamochod()
   {
      return $this->marka.' '.$this->model . 'kolor: ' . $this->kolor;           
   } 
}

$audiExtend = new Audi('A5');
$audiExtend->setKolor('czarny');
echo 'Witaj, jestem '.$audiExtend->pobierzSamochod().PHP_EOL;

