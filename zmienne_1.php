<?php

   $nazwa = 1; // Zmiennej "nazwa" przypisywana jest wartość liczbowa 1

   $druga_nazwa = "Tekst"; // Zmiennej "druga_nazwa" przypisany jest ciąg znaków "Tekst"

   $trzecia_nazwa = $nazwa; // Zmiennej "trzecia_nazwa" przypisywana
                            //jest wartość zmiennej "nazwa"

   echo "To jest $druga_nazwa"; // Powinien wyświetlić się napis "To jest Tekst"

   echo '$druga_nazwa'; // Powinien wyświetlić się napis "$druga_nazwa"

   echo $nazwa; // Powinna wyświetlić się cyfra 1

?>