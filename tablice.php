<?php

$tablica[0] = "Wpis numer 0";
$tablice[1] = "Wpis numer 1";
$tablica[2] = "Wpis numer 2";

echo $tablica[2]; // Wyświetlony zostanie napis "Wpis numer 2";

echo PHP_EOL;
/**
 *  Tablice asocjacyjne
 *
 */
$tablica = array(
  "klucz" => "wartosc",
  "nowy_klucz" => 2
);

//od PHP 5.4
$tablica = ["klucz" => "wartosc", "inny_klucz" => 1];

echo $tablica['inny_klucz'];

echo PHP_EOL;



