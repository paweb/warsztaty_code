<?php
$blah = "0";  // $blah jest ciągiem (ASCII 48)
$blah++;      // $blah jest ciągiem "1" (ASCII 49)
$blah += 1;   // $blah jest teraz wartością całkowitą (2)
$blah = $foo + 1.3;  // $blah jest wartością rzeczywistą (1.3)
$blah = 5 + "10 Malutkich Świnek"; // $blah jest wartością całkowitą (15)
$blah = 5 + "10 Małych Świń";     // $blah jest wartością całkowitą (15)

