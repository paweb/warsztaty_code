<?php

/**
 CREATE TABLE IF NOT EXISTS `osoby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
*/

 try
 {
    $pdo = new PDO('mysql:host=localhost;dbname=warsztaty', 'root', '');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $stmt = $pdo -> query('SELECT id, name, age FROM osoby');
    echo '<ul>';
    while($row = $stmt->fetch())
    {
        echo '<li>'.$row['name'].': '.$row['age'].'</li>';
    }
    $stmt->closeCursor();
    echo '</ul>';
 }
 catch(PDOException $e)
 {
    echo 'Połączenie nie mogło zostać utworzone: ' . $e->getMessage();
 }
